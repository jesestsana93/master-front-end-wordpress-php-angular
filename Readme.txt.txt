Curso de Udemy "Master en Front-End: crea temas para wordpress,PHP, Angular" por Juan Fernando Urrego
Duración: 31 horas

Herramientas:
* https://www.jqueryscript.net/
* http://bootstrap4.guide/
* https://w3layouts.com/
* https://www.jqueryscript.net/slider/Carousel-Slideshow-jdSlider.html
* Video 33 (Paginacion): http://josecebe.github.io/twbs-pagination/
* Video 37 (scrollanimado): https://johnpolacek.github.io/superscrollorama/ 
* Video 37 (flechita para subir al inicio): https://markgoodyear.com/labs/scrollup/
* Video 37: https://easings.net/es#
* Vídeo 91 (Date picker): https://bootstrap-datepicker.readthedocs.io/en/latest/
* Vídeo 106 (vista 360 de una imagen): https://www.jqueryscript.net/other/360-Degree-Panoramic-Image-Viewer-with-jQuery-Pano.html